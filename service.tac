import os
import sys

sys.path.append(os.path.dirname(os.path.realpath(__file__)))

from twisted.application import service
from twisted.internet.task import react
from twisted.logger import (
    FilteringLogObserver,
    LogLevel,
    LogLevelFilterPredicate,
    globalLogPublisher,
    textFileLogObserver,
)
from twisted.python.logfile import LogFile

from src.component import main
from src.settings import Settings

settings = Settings()

# this way all logger messages are sent to stdout
# and saved to log file at log_level

log_level = LogLevel.info

logfile = LogFile(
    name="%s.log" % settings.component_name,
    directory="logs/",
    rotateLength=100000000,  # 100mb
    maxRotatedFiles=1,
)

globalLogPublisher.addObserver(
    FilteringLogObserver(
        textFileLogObserver(logfile), predicates=[LogLevelFilterPredicate(log_level)]
    )
)


class Service(service.Service):
    def startService(self):
        react(main)


application = service.Application(settings.component_name)
service = Service()
service.setServiceParent(application)
