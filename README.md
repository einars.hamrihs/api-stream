# api-stream

An API platform, where both senders and receivers can exchange data as a stream.

## How to run service

* Run the service:

> twistd -ny service.tac

## For receivers:

When receivers connect to api, they receive all incoming data as a stream until they disconnect.
For example:

> curl --no-buffer http://127.0.0.1:8082/listen --output -

## For senders:

When a sender connects to receiver, they can send messages that are delivered to all receivers.
For example:

> telnet 127.0.0.1 3878
> Hello world
> Some test line

## Pre-requisites


> python3.10 -m venv $PROJECT_DIR/venv

> source $_PROJECT_DIR/venv/bin/activate

> pip install -r requirements.txt
