from dotenv import load_dotenv
from pydantic import BaseSettings, Field

load_dotenv(verbose=True)


class Settings(BaseSettings):
    # default values to None for unit tests
    component_name: str = "api-stream"
    api_server_port: int = Field(None, env="api_server_port")
    receiver_port: int = Field(None, env="receiver_port")
