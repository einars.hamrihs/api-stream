from twisted.logger import Logger
from twisted.web.resource import Resource
from twisted.web.server import Request
from twisted.internet import task
from src.services.input_receiver import Lines
from typing import Callable
import uuid


class API(Resource):
    log = Logger()
    api_version = "v1"
    isLeaf = False

    def __init__(self, lines: Lines):
        Resource.__init__(self)
        self.lines = lines

    def cycle(self, echo: Callable, conn_id: str) -> None:
        """
        Checks for newly received lines and sends them to receiver one by one.
        :param echo: A request's method that send back the data.
        :param conn_id: Connection ID.
        :return:
        """
        new_lines = self.lines.get_lines(conn_id)
        for item in new_lines:
            data = item["data"]
            echo(str(f"{data}\n").encode("utf-8"))

    def finished(self, result, conn_id: str, lc: task.LoopingCall) -> None:
        """
        Removes receiver from receivers object, when it disconnects and stops the looping call to stop checking for new lines.
        :param result: Result or reason of successfully or unsuccessfully finished connection.
        :param conn_id: Connection ID
        :param lc: Looping call object
        :return:
        """
        self.log.info(f"{conn_id} disconnected")
        self.log.info(f"Removing receiver {conn_id}")
        self.lines.remove_receiver(conn_id)
        lc.stop()

    async def main_get(self, request: Request) -> None:
        """
        Receives a request from receiver and registers a looping call to receive and send back new lines.
        :param request: Request object
        :return:
        """
        request.responseHeaders.addRawHeader(
            "Content-Type", "text/event-stream; charset=utf-8"
        )
        conn_id = str(uuid.uuid4())
        self.log.info(f"Registering new receiver {conn_id}")
        self.lines.add_receiver(conn_id)

        lc = task.LoopingCall(self.cycle, request.write, conn_id)

        df = request.notifyFinish()
        df.addBoth(self.finished, conn_id, lc)

        await lc.start(1)  # repeat every second
