from twisted.protocols.basic import LineOnlyReceiver
from twisted.internet.protocol import Factory


class DataReceiverProtocol(LineOnlyReceiver):
    def __init__(self, lines):
        self.lines = lines

    def lineReceived(self, line: bytes) -> None:
        line_rep = line.decode(encoding="utf8", errors="replace")
        self.lines.append_line({"data": line_rep})


class DataReceiverFactory(Factory):
    def __init__(self, lines):
        self.receiver = DataReceiverProtocol(lines=lines)

    def buildProtocol(self, addr) -> DataReceiverProtocol:
        return self.receiver


class Lines:
    def __init__(self):
        self.receivers = {}

    def add_receiver(self, conn_id: str) -> None:
        """
        Creates new receiver object with empty lines list for the specified connection ID.
        Every connection has it's own list with lines to be received.
        :param conn_id: connection ID
        :return:
        """
        self.receivers[conn_id] = {"lines": []}

    def append_line(self, line: str) -> None:
        """
        Adds received line to every active receiver's lines list.
        :param line: String with received line
        :return:
        """
        for receiver in self.receivers.values():
            receiver["lines"].append(line)

    def get_lines(self, conn_id: str):
        """
        Returns an object with newly received lines, when receiver asks.
        :param conn_id: Connection ID
        :return: List of newly received lines
        """
        lines = self.receivers[conn_id]["lines"]
        self.receivers[conn_id]["lines"] = []
        return lines

    def remove_receiver(self, conn_id: str) -> None:
        """
        Removes receiver from receivers object, when it disconnects.
        :param conn_id: Connection ID
        :return:
        """
        del self.receivers[conn_id]
