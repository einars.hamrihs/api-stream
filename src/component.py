from twisted.logger import Logger
from twisted.web.server import Site
from twisted.internet.endpoints import TCP4ServerEndpoint
from twisted.internet import defer, reactor
from klein import Klein
from src.services.apistream_service import API
from src.services.input_receiver import DataReceiverFactory, Lines

from src.settings import Settings

settings = Settings()


class Component:
    log = Logger()

    def __init__(self, app):

        self._app = app
        self._session = None
        self.metrics_server = None
        self.api_listener = None
        self.receiver = None
        self.lines = None

        self.register_services()

    def register_services(self):
        self.log.info("registering services..")

        self.lines = Lines()
        api = API(lines=self.lines)

        self.log.info("service running..")

        self._app.route("/listen", methods=["GET"], branch=True)(api.main_get)

        self.receiver = reactor.listenTCP(
            settings.receiver_port, DataReceiverFactory(lines=self.lines)
        )


async def main(reactor):
    web_app = Klein()
    Component(web_app)

    site = Site(web_app.resource())
    server_ep = TCP4ServerEndpoint(reactor, settings.api_server_port)
    await server_ep.listen(site)

    # we don't *have* to hand over control of the reactor to
    # component.run -- if we don't want to, we call .start()
    # The Deferred it returns fires when the component is "completed"
    # (or errbacks on any problems).
    # log = Logger()

    # If the Component raises an exception we want to exit. Note that
    # things like failing to connect will be swallowed by the
    # re-connection mechanisms already so won't reach here.

    # wait forever (unless the Component raises an error)
    done = defer.Deferred()
    await done
