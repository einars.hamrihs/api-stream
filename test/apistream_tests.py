from twisted.trial import unittest
from src.services.apistream_service import API
from src.services.input_receiver import Lines
from twisted.internet import task, defer
import mock


class TestAPI(unittest.TestCase):
    def setUp(self):
        self.api = API(lines=Lines())

    def test_cycle(self):
        with mock.patch.object(
            Lines,
            "get_lines",
            return_value=[{"data": "hello world"}, {"data": "line 2"}],
        ):
            mock_request = mock.Mock()
            self.api.cycle(mock_request, "1")
            mock_request.assert_any_call("hello world\n".encode("utf-8"))
            mock_request.assert_any_call("line 2\n".encode("utf-8"))

    def test_finished(self):
        with mock.patch.object(
            Lines, "remove_receiver", return_value=None
        ) as mock_remove_receiver:
            mock_lc = mock.Mock()
            with mock.patch.object(mock_lc, "stop", return_value=None) as mock_stop_lc:
                self.api.finished("success", "1", mock_lc)
                mock_remove_receiver.assert_called_with("1")
                mock_stop_lc.assert_called()

    @defer.inlineCallbacks
    def test_main_get(self):
        request_mock = mock.MagicMock()
        request_mock.responseHeaders.addRawHeader.return_value = None
        request_mock.notifyFinish.return_value = defer.Deferred()
        with mock.patch.object(
            task.LoopingCall, "start", return_value=defer.succeed(None)
        ) as mock_lc:
            mock_lc.start.return_value = None
            coro = self.api.main_get(request_mock)
            result = yield defer.Deferred.fromCoroutine(coro)
            self.assertIsNone(result)
