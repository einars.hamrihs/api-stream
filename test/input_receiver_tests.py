from twisted.trial import unittest
from src.services.input_receiver import Lines, DataReceiverProtocol
import mock


class TestInputReceiver(unittest.TestCase):
    def setUp(self):
        self.lines = Lines()
        self.protocol = DataReceiverProtocol(self.lines)

    def test_add_receiver(self):
        result = self.lines.add_receiver("1")
        self.assertIsNone(result)

    def test_append_line(self):
        result = self.protocol.lineReceived("test".encode("utf-8"))
        self.assertIsNone(result)

    def test_get_lines(self):
        with mock.patch.object(
            Lines, "get_lines", return_value={"data": "test"}
        ) as get_lines_mock:
            result = self.lines.get_lines("1")
            self.assertEqual(result, {"data": "test"})
            get_lines_mock.assert_called_with("1")

    def test_remove_receiver(self):
        with mock.patch.object(
            Lines, "remove_receiver", return_value=None
        ) as remove_mock:
            result = self.lines.remove_receiver("1")
            self.assertIsNone(result)
            remove_mock.assert_called_with("1")

    def test_add_get_and_remove(self):
        self.lines.add_receiver("1")
        self.lines.add_receiver("2")
        self.protocol.lineReceived("test".encode("utf-8"))
        result1 = self.lines.get_lines("1")
        result2 = self.lines.get_lines("2")
        self.lines.remove_receiver("1")
        self.lines.remove_receiver("2")

        self.assertEqual(result1, [{"data": "test"}])
        self.assertEqual(result2, [{"data": "test"}])
