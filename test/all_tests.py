from twisted.trial import unittest
from test.apistream_tests import TestAPI
from test.input_receiver_tests import TestInputReceiver


class TestAll(unittest.TestCase):
    def test_all(self):
        testSuite = unittest.TestSuite()
        testSuite.run(TestAPI)
        testSuite.run(TestInputReceiver)
