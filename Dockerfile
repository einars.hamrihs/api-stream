FROM python:3.9-buster as builder

RUN apt-get update \
&& apt-get install libxml2-dev libxslt1-dev -y \
&& apt-get clean

COPY requirements.txt code/
COPY service.tac code/
WORKDIR /code

ENV VIRTUAL_ENV=/code/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

ADD src/. /code/src

# PRODUCTION IMAGE
FROM python:3.9-slim-buster
COPY --from=builder /code /code
WORKDIR /code
ENV VIRTUAL_ENV=/code/venv
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

CMD [ "twistd", "--pidfile=", "--prefix=livedata", "-ny", "service.tac" ]
